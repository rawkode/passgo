package main

import (
	"gitlab.com/rawkode/passgo/commands/vault"
	"gopkg.in/urfave/cli.v2"
	"os"
)

func main() {
	app := &cli.App{
		Name:  "passgo",
		Usage: "passgo",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "region",
				Value: "eu-west-1",
				Usage: "Region to use (Defaults to eu-west-1)",
			},
		},
		Commands: []*cli.Command{
			{
				Name:        "vault",
				Usage:       "vault",
				Description: "Vault",
				Subcommands: []*cli.Command{
					{
						Name:   "create",
						Action: vault.Create,
					},
				},
			},
		},
	}

	app.Run(os.Args)
}
