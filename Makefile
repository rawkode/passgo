# Project Information
##
VERSION    = $(shell grep "const Version " $(TOP)/version.go | sed -E 's/.*"(.+)"$$/\1/')

# Build information
##
OUTPUT    = passgo
SRCDIR 		= ./src
BUILDDIR  = ./pkg
VENDORDIR = .//vendor
XC_OS     = "darwin linux windows"
XC_ARCH   = "386 amd64"
DISTDIR   = $(BUILDDIR)/dist/$(VERSION)

# Formatting Symbols
##
H1 				= "====="
H2 				= "===="

# Commands
##
help:
	@echo "${H1} Coming Soon"

test: deps
	@echo "${H1} Running tests..."
	go test -v ./...

setup:
	@echo "${H1} Preparing Development Environment"
	go get -u github.com/golang/dep/cmd/dep
	go get -u github.com/mitchellh/gox
	go get -u github.com/tcnksm/ghr

install: deps
	@echo "${H1} Installing '$(OUTPUT)' to $(GOPATH)/bin..."
	go build -o $(GOPATH)/bin/$(OUTPUT)

deps:
	@echo "${H1} Installing runtime dependencies..."
	dep ensure

deps.update:
	@echo "${H1} Updating runtime dependencies..."
	dep ensure -update

build: deps
	@echo "${H1} Beginning compile..."
	@cd ${SRCDIR}
	gox -os $(XC_OS) -arch $(XC_ARCH) -output "pkg/{{.OS}}_{{.Arch}}/$(OUTPUT)"

dist: build
	@echo "${H1} Shipping packages..."
	rm -rf $(DISTDIR)
	mkdir -p $(DISTDIR)
	@for dir in $$(find $(BUILDDIR) -mindepth 1 -maxdepth 1 -type d); do \
		platform=`basename $$dir`; \
		if [ $$platform = "dist" ]; then \
			continue; \
		fi; \
		archive=$(OUTPUT)_$(VERSION)_$$platform; \
		zip -j $(DISTDIR)/$$archive.zip $$dir/*; \
		pushd $(DISTDIR); \
		shasum -a 256 *.zip > ./$(VERSION)_SHA256SUMS; \
		popd; \
	done

clean:
	go clean ./...
	rm -rf $(VENDORDIR)
	rm -rf $(BUILDDIR)

.PHONY: help test setup deps deps.update clean
