package vault

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"gopkg.in/urfave/cli.v2"
)

func Create(c *cli.Context) error {
	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(c.String("region")),
	}))

	s3Client := s3.New(sess)

	bucketName := c.Args().First()

	params := &s3.CreateBucketInput{
		Bucket: aws.String(bucketName),
	}

	_, err := s3Client.CreateBucket(params)

	if err != nil {
		fmt.Println("Some error: ", err)
	}
	return nil
}
